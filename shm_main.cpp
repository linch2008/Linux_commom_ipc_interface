#include <iostream>
#include "commom_ipc.h"
#include <pthread.h>
#include <cstdio>

using namespace std;

#define KEY 0x12345678

void *thread1(void *arg)
{
    ShareMemory my_shm(KEY);
    ShareMemoryData shm;
    shm.valid_size = 20;
    memcpy(shm.buffer, "Hello, World!", shm.valid_size);
    my_shm.set_shm(&shm);

    return (void*)0;
}

void *thread2(void *arg)
{
    sleep(1);
    ShareMemory my_shm(KEY);
    ShareMemoryData shm;

    my_shm.get_shm(&shm);
    cout<< "valid_size: "<<shm.valid_size<<endl;
    cout<< "buffer: "<<shm.buffer<<endl;

    return (void*)0;
}


int main()
{

    pthread_t id1;
    pthread_t id2;

    if (pthread_create(&id1, NULL, thread1, NULL) == -1)
    {
        printf("pthread_create failed.\n");
    }

    if (pthread_create(&id2, NULL, thread2, NULL) == -1)
    {
        printf("pthread_create failed.\n");
    }

    getchar();
    return 0;
}