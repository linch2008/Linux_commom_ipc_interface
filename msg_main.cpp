#include <iostream>
#include "commom_ipc.h"
#include <pthread.h>
#include <cstdio>

using namespace std;

#define KEY 0x12345678

void *thread1(void *arg)
{
    Message my_msg(KEY);
    MessageData msg;

    msg.m_type = 1;
    msg.ImportBuffer("Hello, World!");
    my_msg.set_message(&msg);
    my_msg.send_msg();

    my_msg.recv_msg(4);
    my_msg.get_message(&msg);
    cout<< "type: "<<msg.m_type<<endl;
    cout<< "recv: "<<msg.m_buffer<<endl;

    return (void*)0;
}

void *thread2(void *arg)
{
    Message my_msg(KEY);
    MessageData msg;

    my_msg.recv_msg(1);
    my_msg.get_message(&msg);
    cout<< "type: "<<msg.m_type<<endl;
    cout<< "recv: "<<msg.m_buffer<<endl;

    msg.m_type = 4;
    msg.ImportBuffer("222222222222222");
    my_msg.set_message(&msg);
    my_msg.send_msg();

    return (void*)0;
}


int main()
{

    pthread_t id1;
    pthread_t id2;

    if (pthread_create(&id1, NULL, thread1, NULL) == -1)
    {
        printf("pthread_create failed.\n");
    }

    if (pthread_create(&id2, NULL, thread2, NULL) == -1)
    {
        printf("pthread_create failed.\n");
    }

    getchar();
    return 0;
}