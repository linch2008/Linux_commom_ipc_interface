CXX := g++ 
CXXFLAGS := -Wall 
LIBS := -lpthread
SRCS := commom_ipc.cpp sockdemo.cpp msg_main.cpp sem_shm.cpp shm_main.cpp
OBJS := $(addsuffix .o,$(basename $(SRCS)))

OUTPUT := sockdemo msg_main sem_shm shm_main

all:$(OUTPUT)
        
sockdemo:commom_ipc.o sockdemo.o
    $(CXX) $(CXXFLAGS) $(LIBS) -o $@ $^
msg_main:commom_ipc.o msg_main.o
	$(CXX) $(CXXFLAGS) $(LIBS) -o $@ $^
sem_shm:commom_ipc.o sem_shm.o
	$(CXX) $(CXXFLAGS) $(LIBS) -o $@ $^
shm_main:commom_ipc.o shm_main.o
	$(CXX) $(CXXFLAGS) $(LIBS) -o $@ $^
	
%.o : %.cpp
	$(CXX) -c $(CXXFLAGS) $(LIBS) -o $@ $<
clean:
	rm -fr $(OUTPUT) $(OBJS)
	